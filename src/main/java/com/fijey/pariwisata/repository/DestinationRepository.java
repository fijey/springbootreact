package com.fijey.pariwisata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fijey.pariwisata.model.Destination;

@Repository
public interface DestinationRepository extends JpaRepository<Destination, Long> {

}
