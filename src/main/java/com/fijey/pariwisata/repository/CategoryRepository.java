package com.fijey.pariwisata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fijey.pariwisata.model.Category;


@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	

}
