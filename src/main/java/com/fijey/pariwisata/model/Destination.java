package com.fijey.pariwisata.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "destinations")
public class Destination {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)private long id;
	@Column(nullable = true,name = "destination_name" ) private String destinationName;
	@Column(nullable = true,name = "destination_price") private Double destinationPrice;
	@Column(nullable = true,name = "destination_detail")private String destinationDetail;
	@Column(nullable = true,name = "destination_city")	private String destinationCity;
	@Column(nullable = true,name = "destination_address")private String destinationAdress;
	@Column(nullable = true,name = "destination_rating") private Double destinationRating;
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id", referencedColumnName = "categoryId")
    private Category destinationCategory;
	@Column(nullable = true,name = "img_banner") private String imgBanner;
	@Column(nullable = true,name="img_testi1") private String imgTesti1;
	@Column(nullable = true,name="img_test2") private String imgTesti2;
	@Column(nullable = true,name="img_testi3") private String imgTesti3;
	@Column(nullable = true,name = "img_cap1") private String imgCaption1;
	@Column(nullable = true,name = "img_cap2") private String imgCaption2;
	@Column(nullable = true,name = "img_cap3")	private String imgCaption3;
	@Column(nullable = true,name = "healty_protocol")private Boolean healtyProtocol;
	@Column(nullable = true,name = "protocol_description")private String protocolDescription;

	public Destination() {
		super();
		
	}
	
	public Destination(String destinationName, Double destinationPrice, String destinationDetail) {
		super();
		this.destinationName = destinationName;
		this.destinationPrice = destinationPrice;
		this.destinationDetail = destinationDetail;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	

	

	public Category getDestinationCategory() {
		return destinationCategory;
	}

	public void setDestinationCategory(Category destinationCategory) {
		this.destinationCategory = destinationCategory;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public Double getDestinationPrice() {
		return destinationPrice;
	}

	public void setDestinationPrice(Double destinationPrice) {
		this.destinationPrice = destinationPrice;
	}

	public String getDestinationDetail() {
		return destinationDetail;
	}

	public void setDestinationDetail(String destinationDetail) {
		this.destinationDetail = destinationDetail;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	

	public String getDestinationAdress() {
		return destinationAdress;
	}

	public void setDestinationAdress(String destinationAdress) {
		this.destinationAdress = destinationAdress;
	}

	public Double getDestinationRating() {
		return destinationRating;
	}

	public void setDestinationRating(Double destinationRating) {
		this.destinationRating = destinationRating;
	}

	public String getImgBanner() {
		return imgBanner;
	}

	public void setImgBanner(String imgBanner) {
		this.imgBanner = imgBanner;
	}

	public String getImgTesti1() {
		return imgTesti1;
	}

	public void setImgTesti1(String imgTesti1) {
		this.imgTesti1 = imgTesti1;
	}

	public String getImgTesti2() {
		return imgTesti2;
	}

	public void setImgTesti2(String imgTesti2) {
		this.imgTesti2 = imgTesti2;
	}

	public String getImgTesti3() {
		return imgTesti3;
	}

	public void setImgTesti3(String imgTesti3) {
		this.imgTesti3 = imgTesti3;
	}

	public String getImgCaption1() {
		return imgCaption1;
	}

	public void setImgCaption1(String imgCaption1) {
		this.imgCaption1 = imgCaption1;
	}

	public String getImgCaption2() {
		return imgCaption2;
	}

	public void setImgCaption2(String imgCaption2) {
		this.imgCaption2 = imgCaption2;
	}

	public String getImgCaption3() {
		return imgCaption3;
	}

	public void setImgCaption3(String imgCaption3) {
		this.imgCaption3 = imgCaption3;
	}

	public Boolean getHealtyProtocol() {
		return healtyProtocol;
	}

	public void setHealtyProtocol(Boolean healtyProtocol) {
		this.healtyProtocol = healtyProtocol;
	}

	public String getProtocolDescription() {
		return protocolDescription;
	}

	public void setProtocolDescription(String protocolDescription) {
		this.protocolDescription = protocolDescription;
	}
	
	
	
	
	
	

}
