package com.fijey.pariwisata.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fijey.pariwisata.exception.ResourceNotFoundException;
import com.fijey.pariwisata.model.Destination;
import com.fijey.pariwisata.repository.DestinationRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class DestinationController {
	@Autowired
	private DestinationRepository destinationRepository;
	
	//getAllDestination
	
	@GetMapping("/destination")
	public List<Destination> getAllDestinations(){
		return destinationRepository.findAll();
	}
	
	//createDestinations
	@PostMapping("/destination")
	public Destination createDestination(@RequestBody Destination destination) {
		return destinationRepository.save(destination);
	}
	
	//getDestinationById
	@GetMapping("destination/{id}")
	public ResponseEntity<Destination> getDestinationById(@PathVariable(value = "id") Long id) {
		Destination destination = destinationRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Destination not Exist with Id " + id));
		return ResponseEntity.ok(destination);
	}

	//updateDestination
	@PutMapping("/destination/{id}")
	public ResponseEntity<Destination> updateDestination(@PathVariable(value ="id") Long id, @RequestBody Destination destinationDetails ){
		Destination destination = destinationRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Destination not Exist with Id " + id));
		
		destination.setDestinationName(destinationDetails.getDestinationName());
		destination.setDestinationPrice(destinationDetails.getDestinationPrice());
		destination.setDestinationDetail(destinationDetails.getDestinationDetail());
		destination.setDestinationCity(destinationDetails.getDestinationCity());
		destination.setDestinationAdress(destinationDetails.getDestinationAdress());
		destination.setDestinationRating(destinationDetails.getDestinationRating());
		destination.setDestinationCategory(destinationDetails.getDestinationCategory());
//		destination.setImgBanner(destinationDetails.getImgBanner());
//		destination.setImgTesti1(destinationDetails.getImgTesti1());
//		destination.setImgTesti2(destinationDetails.getImgTesti2());
//		destination.setImgTesti3(destinationDetails.getImgTesti3());
//		destination.setImgCaption1(destinationDetails.getImgCaption1());
//		destination.setImgCaption2(destinationDetails.getImgCaption2());
//		destination.setImgCaption3(destinationDetails.getImgCaption3());
//		destination.setHealtyProtocol(destinationDetails.getHealtyProtocol());
//		destination.setProtocolDescription(destinationDetails.getProtocolDescription());
		
		
	Destination updateDestination =	destinationRepository.save(destination);
	
		return ResponseEntity.ok(updateDestination);
		
	}
	
	//deleteDestination
	@DeleteMapping("/destination/{id}")
	public ResponseEntity<Map<String,Boolean>> deleteDestination (@PathVariable Long id){
		
		Destination destination = destinationRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Destination not exist with Id " + id));
		
		destinationRepository.delete(destination);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return ResponseEntity.ok(response);
		
	}
	
}
