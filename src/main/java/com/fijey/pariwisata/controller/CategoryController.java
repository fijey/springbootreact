package com.fijey.pariwisata.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fijey.pariwisata.model.Category;
import com.fijey.pariwisata.repository.CategoryRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class CategoryController {
	@Autowired
	private CategoryRepository categoryRepository;
	
	//ambil kategory
	@GetMapping("/category")
	public List<Category> getCategory(){
		return categoryRepository.findAll();
	}
	
	//buatcategory
	@PostMapping("/category")
	public Category createCategory(@Valid @RequestBody Category category) {
		categoryRepository.save(category);
		return category;
		
	}

}
