package com.fijey.pariwisata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PariwisataApplication {

	public static void main(String[] args) {
		SpringApplication.run(PariwisataApplication.class, args);
	}

}
